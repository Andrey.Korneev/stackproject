template <class T, int max>
class Stack {
public:
	Stack();
	bool is_empty();
	bool is_full();
	bool push(const T& item);
	bool pop(T& item);
private:
	T items[max];
	int top;
};

template <class T, int max>
Stack<T, max>::Stack() {
	top = 0;
}

template <class T, int max>
bool Stack<T, max>::is_empty() {
	return top == 0;
}

template <class T, int max>
bool Stack<T, max>::is_full() {
	return top == max;
}

template <class T, int max>
bool Stack<T, max>::push(const T& item) {
	if (is_full()) return false;
	items[top++] = item;
	return true;
}

template <class T, int max>
bool Stack<T, max>::pop(T& item) {
	if (is_empty()) return false;
	item = items[--top];
	return true;
}