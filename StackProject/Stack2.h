template <class T>
class Stack2 {
public:
	Stack2(int size);
	~Stack2();
	bool is_empty();
	bool is_full();
	bool push(const T& item);
	bool pop(T& item);
private:
	T* items;
	int MAX;
	int top;
};

template <class T>
Stack2<T>::Stack2(int size) {
	items = new T[size];
	MAX = size;
	top = 0;
}

template <class T>
Stack2<T>::~Stack2() {
	delete[] items;
}

template <class T>
bool Stack2<T>::is_empty() {
	return top == 0;
}

template <class T>
bool Stack2<T>::is_full() {
	return top == MAX;
}

template <class T>
bool Stack2<T>::push(const T& item) {
	if (is_full()) return false;
	items[top++] = item;
	return true;	
}

template <class T>
bool Stack2<T>::pop(T& item) {
	if (is_empty()) return false;
	item = items[--top];
	return true;	
}