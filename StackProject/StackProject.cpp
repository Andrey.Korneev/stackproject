#include <iostream>
#include <string>
#include "Stack.h"
#include "Stack2.h"

int main() {

    //--Stack-------------------------

    Stack<int, 10> IntStack10;
    int i = 1;
    while (IntStack10.push(i++));
    while (IntStack10.pop(i)) {
        std::cout << i << std::endl;
    }
    std::cout << std::endl;

    Stack<float, 8> FloatStack8;
    float f = 1.0f;
    while (FloatStack8.push(f)) {
        f += 0.5f;
    }
    std::cout.precision(2);
    std::cout.setf(std::ios_base::showpoint);
    while (FloatStack8.pop(f)) {
        std::cout << f << std::endl;
    }
    std::cout << std::endl;


    //--Stack2-------------------------

    Stack2<std::string> StringStack5(5);
    StringStack5.push("world!");
    StringStack5.push("crazy");
    StringStack5.push("crazy");
    StringStack5.push("crazy");
    StringStack5.push("This");
    std::string word;
    while (StringStack5.pop(word)) {
        std::cout << word << ' ';
    }
    std::cout << std::endl;

    return 0;
}